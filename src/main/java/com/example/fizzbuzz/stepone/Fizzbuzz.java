package com.example.fizzbuzz.stepone;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Fizzbuzz {
	
	public static void main(String args[]) throws IOException {
		if(args.length != 2) {
			throw new IllegalArgumentException("Unexpected arguments.");
		}
		
		String result = new Fizzbuzz().calculate(Integer.valueOf(args[0]), Integer.valueOf(args[1]));
		
		System.out.write(result.getBytes());
	}
	
	public Fizzbuzz() {
	}

	public String calculate(int start, int end) {
		if (end < start) {
			throw new IllegalArgumentException("End cannot be smaller than start");
		}

		List<String> result = new ArrayList<>();
		for (int i = start; i <= end; i++) {
			if (isMultiplierOf15(i)) {
				result.add("fizzbuzz");
			} else if (isMultiplierOf5(i)) {
				result.add("buzz");
			} else if (isMultiplierOf3(i)) {
				result.add("fizz");
			} else {
				result.add(String.valueOf(i));
			}
		}

		return getOutput(result);
	}

	private boolean isMultiplierOf15(int i) {
		return i % 15 == 0;
	}

	private boolean isMultiplierOf5(int i) {
		return i % 5 == 0;
	}

	private boolean isMultiplierOf3(int i) {
		return i % 3 == 0;
	}
	
	private String getOutput(List<String> result) {
		StringBuffer output = new StringBuffer();
		for (String string : result) {
			output.append(string).append(" ");
		}
		
		return output.toString().trim();
	}
}
