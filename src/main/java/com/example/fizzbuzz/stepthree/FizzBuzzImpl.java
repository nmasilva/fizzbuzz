package com.example.fizzbuzz.stepthree;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.IntFunction;

/**
 * Implementation based java 8 functions and decorator pattern of Fizzbuzz interface
 */
class FizzBuzzImpl implements Fizzbuzz {
	private static final String EMPTY_SPACE = " ";
	private static final String LINE_RETURN = "\n";
	
	private final Iterable<FizzbuzzFunctionDecorator> fizzbuzzFuntions;
	private final Iterable<FizzbuzzFunctionDecorator> funtionsCountOrder;

	public FizzBuzzImpl(FizzbuzzFunctionFactory factory) {
		FizzbuzzFunctionDecorator lucky = new FizzbuzzFunctionDecorator("lucky", factory.createPresenceFunction(3, "lucky"));
		FizzbuzzFunctionDecorator fizzbuzz = new FizzbuzzFunctionDecorator("fizzbuzz", factory.createMultiplierFunction(15, "fizzbuzz"));
		FizzbuzzFunctionDecorator buzz = new FizzbuzzFunctionDecorator("buzz", factory.createMultiplierFunction(5, "buzz"));
		FizzbuzzFunctionDecorator fizz = new FizzbuzzFunctionDecorator("fizz", factory.createMultiplierFunction(3, "fizz"));
		FizzbuzzFunctionDecorator integer = new FizzbuzzFunctionDecorator("integer", factory.createDefaultFunction());

		fizzbuzzFuntions = Arrays.asList(lucky, fizzbuzz, buzz, fizz, integer);
		funtionsCountOrder = Arrays.asList(fizz, buzz, fizzbuzz, lucky, integer);
	}

	@Override
	public void calculate(int start, int end, OutputStream outputStream) throws FizzbuzzException {
		if (end < start) {
			throw new IllegalArgumentException("End cannot be smaller than start");
		}

		try {
			for (int i = start; i <= end; i++) {
				processNumber(i, outputStream);
			}
			outputStream.write(getCountOutput().getBytes());
		} catch (IOException e) {
			throw new FizzbuzzException(e);
		}
	}

	
	/**
	 * Iterate by all function by the order of precedence until it finds a match.
	 * @param inputNumber the number to be processed.
	 * @param outputStream the outputStream to write the result.
	 * @throws IOException in case of an error.
	 */
	private void processNumber(int inputNumber, OutputStream outputStream) throws IOException {
		for (IntFunction<Optional<String>> function : fizzbuzzFuntions) {
			Optional<String> fizzbuzzResult = function.apply(inputNumber);
			if (fizzbuzzResult.isPresent()) {
				outputStream.write(fizzbuzzResult.get().concat(EMPTY_SPACE).getBytes());
				break;
			}
		}
	}

	private String getCountOutput() {
		StringBuilder output = new StringBuilder(LINE_RETURN);
		for (FizzbuzzFunctionDecorator functionDecorator : funtionsCountOrder) {
			output.append(functionDecorator.getName()).append(": ").append(functionDecorator.getCount()).append(LINE_RETURN);
		}

		return output.toString();
	}
}
