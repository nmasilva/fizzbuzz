package com.example.fizzbuzz.stepthree;

import java.io.OutputStream;

/**
 *	Given a start and end number, iterate for all the number in the range. Apply the fizzbuzz algorithm and write the output.
 */
public interface Fizzbuzz {
	void calculate(int start, int end, OutputStream outputStream) throws FizzbuzzException;
}
