package com.example.fizzbuzz.stepthree;

/**
 * Fizzbuzz business exception.
 */
public class FizzbuzzException extends Exception {
	private static final long serialVersionUID = -2503693826074930621L;

	public FizzbuzzException(Throwable t) {
		super(t);
	}
}