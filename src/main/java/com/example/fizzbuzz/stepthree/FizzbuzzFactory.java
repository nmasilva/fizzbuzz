package com.example.fizzbuzz.stepthree;

/**
 * Fizzbuzz factory to create fizzbuzz implementations.
 */
public class FizzbuzzFactory {
	public Fizzbuzz createFizzbuzz() {
		return new FizzBuzzImpl(new FizzbuzzFunctionFactory());
	}
}
