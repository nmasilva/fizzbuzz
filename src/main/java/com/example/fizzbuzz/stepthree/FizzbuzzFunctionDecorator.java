package com.example.fizzbuzz.stepthree;

import java.util.Optional;
import java.util.function.IntFunction;

class FizzbuzzFunctionDecorator implements IntFunction<Optional<String>> {
	private int count;
	private final String name;
	private final IntFunction<Optional<String>> function;
	
	public FizzbuzzFunctionDecorator(String name, IntFunction<Optional<String>> function) {
		this.name = name;
		this.function = function;
	}

	@Override
	public Optional<String> apply(int t) {
		Optional<String> optional = function.apply(t);
		if(optional.isPresent()) {
			count++;
		}
		
		return optional;
	}
	
	public String getName() {
		return name;
	}
	
	public int getCount() {
		return count;
	}
}