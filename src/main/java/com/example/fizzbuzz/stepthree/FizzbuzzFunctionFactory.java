package com.example.fizzbuzz.stepthree;

import java.util.Optional;
import java.util.function.IntFunction;

/**
 * Factory that create the login block of fizzbuzz modeled in java 8 Function
 * interfaces.
 */
class FizzbuzzFunctionFactory {

	/**
	 * Create the implementation of a function that detects if a number is contained
	 * in another number.
	 * 
	 * @param targetNumber
	 *            the number to be checked if is contained in the source number.
	 * @param token
	 *            the string result in case of a match.
	 * @return the function interface implementation.
	 */
	public IntFunction<Optional<String>> createPresenceFunction(final int targetNumber, final String token) {
		return sourceNumber -> {
			Optional<String> result = Optional.empty();
			if (containsNumber(sourceNumber, targetNumber)) {
				result = Optional.of(token);
			}

			return result;
		};
	}

	/**
	 * Create the implementation of a function that detects if a number is a
	 * multiplier of another number.
	 * 
	 * @param multiplier
	 *            the multiplier number
	 * @param token
	 *            the string result in case of a match.
	 * @return the function interface implementation.
	 */
	public IntFunction<Optional<String>> createMultiplierFunction(final int multiplier, final String token) {
		return inputNumber -> {
			Optional<String> result = Optional.empty();
			if (inputNumber % multiplier == 0) {
				result = Optional.of(token);
			}

			return result;
		};
	}

	/**
	 * Creates the default implementation that returns the string representation of
	 * the input number.
	 * 
	 * @return the function interface implementation.
	 */
	public IntFunction<Optional<String>> createDefaultFunction() {
		return inputNumber -> Optional.of(String.valueOf(inputNumber));
	}

	private boolean containsNumber(int number, int numberToSearch) {
		boolean containsNumber = false;
		while (number > 0) {
			if (containsNumber = (number % 10 == numberToSearch)) {
				break;
			}
			number = number / 10;
		}

		return containsNumber;
	}
}
