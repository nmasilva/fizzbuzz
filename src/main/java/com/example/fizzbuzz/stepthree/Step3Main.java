package com.example.fizzbuzz.stepthree;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Step3Main {
	private static final Logger LOG = Logger.getLogger(Step3Main.class.getName());
	
	public static void main(String[] args) {
		if(args.length != 2) {
			throw new IllegalArgumentException("Expected 2 parameters, found:" + args.length);
		}
		
		try {
			new Step3Main(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		} catch (NumberFormatException | FizzbuzzException ex) {
			LOG.log(Level.SEVERE, "Exception occur", ex);
		}
	} 
	
	public Step3Main(int start, int end) throws FizzbuzzException {
		new FizzbuzzFactory().createFizzbuzz().calculate(start, end, System.out);
	}
}
