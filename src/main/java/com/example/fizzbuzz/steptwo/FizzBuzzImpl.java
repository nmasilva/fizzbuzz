package com.example.fizzbuzz.steptwo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

class FizzBuzzImpl implements Fizzbuzz {

	private List<Function<Integer, Optional<String>>> funtions;

	public FizzBuzzImpl(FizzbuzzFactory factory) {
		funtions = new ArrayList<>();
		funtions.add(factory.createPresenceFunction(3, "lucky"));
		funtions.add(factory.createMultiplierFunction(15, "fizzbuzz"));
		funtions.add(factory.createMultiplierFunction(5, "buzz"));
		funtions.add(factory.createMultiplierFunction(3, "fizz"));
		funtions.add(factory.createDefaultFunction());
	}

	@Override
	public String calculate(int start, int end) {
		if (end < start) {
			throw new IllegalArgumentException("End cannot be smaller than start");
		}

		List<String> result = new ArrayList<>();
		for (int i = start; i <= end; i++) {
			for (Function<Integer, Optional<String>> function : funtions) {
				Optional<String> apply = function.apply(i);
				if(apply.isPresent()) {
					result.add(apply.get());
					break;
				}
			}
		}

		return getOutput(result);
	}

	private String getOutput(List<String> result) {
		StringBuffer output = new StringBuffer();
		for (String string : result) {
			output.append(string).append(" ");
		}

		return output.toString().trim();
	}
	
	public static void main(String args[]) throws IOException {
		if(args.length != 2) {
			throw new IllegalArgumentException("Unexpected arguments.");
		}
		
		String result = new FizzBuzzImpl(new FizzbuzzFactory()).calculate(Integer.valueOf(args[0]), Integer.valueOf(args[1]));
		
		System.out.write(result.getBytes());
	}
}
