package com.example.fizzbuzz.steptwo;

public interface Fizzbuzz {
	String calculate(int start, int end);
}
