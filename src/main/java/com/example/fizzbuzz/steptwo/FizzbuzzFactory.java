package com.example.fizzbuzz.steptwo;

import java.util.Optional;
import java.util.function.Function;

class FizzbuzzFactory {

	public Function<Integer, Optional<String>> createPresenceFunction(final int number, final String token) {
		return new Function<Integer, Optional<String>>() {
			@Override
			public Optional<String> apply(Integer i) {
				Optional<String> result = Optional.empty();
				if (i.toString().contains(String.valueOf(number))) {
					result = Optional.of(token);
				}

				return result;
			}
		};
	}
	
	public Function<Integer, Optional<String>> createMultiplierFunction(final int multiplier, final String token) {
		return new Function<Integer, Optional<String>>() {
			@Override
			public Optional<String> apply(Integer i) {
				Optional<String> result = Optional.empty();
				if (i % multiplier == 0) {
					result = Optional.of(token);
				}

				return result;
			}
		};
	}

	public Function<Integer, Optional<String>> createDefaultFunction() {
		return new Function<Integer, Optional<String>>() {
			@Override
			public Optional<String> apply(Integer i) {
				return Optional.of(i.toString());
			}
		};
	}
}
