package com.example.fizzbuzz.stepone;

import org.junit.Assert;
import org.junit.Test;

import com.example.fizzbuzz.stepone.Fizzbuzz;


public class FizzbuzzTest {

	@Test
	public void positiveRangeSucess() {
		Assert.assertEquals("2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz 16", new Fizzbuzz().calculate(2, 16));
	}
	
	@Test
	public void negativeRangeSucess() {
		Assert.assertEquals("fizz -2 -1", new Fizzbuzz().calculate(-3, -1));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void rangeException() {
		Fizzbuzz fizzbuzz = new Fizzbuzz();
		fizzbuzz.calculate(3, 1);
	}
}