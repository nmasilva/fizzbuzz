package com.example.fizzbuzz.stepthree;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.junit.Assert;
import org.junit.Test;

public class FizzBuzzImplTest {
	@Test
	public void positiveRangeSucess() throws Exception {
		OutputStream outStream = new ByteArrayOutputStream();
		new FizzbuzzFactory().createFizzbuzz().calculate(1, 20, outStream);
		String result = outStream.toString();
		
		Assert.assertEquals("1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz \n"
				+ "fizz: 4\n"
				+ "buzz: 3\n"
				+ "fizzbuzz: 1\n"
				+ "lucky: 2\n"
				+ "integer: 10\n", result);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void rangeException() throws Exception {
		new FizzBuzzImpl(new FizzbuzzFunctionFactory()).calculate(3, 1, System.out);
	}
	
	@Test(expected=FizzbuzzException.class)
	public void fizzbuzzException() throws Exception {
		OutputStream outStream = new ByteArrayOutputStream() {
			@Override
			public void write(byte b[]) throws IOException {
				throw new IOException();
			}
		};
		new FizzbuzzFactory().createFizzbuzz().calculate(1, 20, outStream);
	}
}