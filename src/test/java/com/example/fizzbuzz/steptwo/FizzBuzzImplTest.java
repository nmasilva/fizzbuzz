package com.example.fizzbuzz.steptwo;

import org.junit.Assert;
import org.junit.Test;

public class FizzBuzzImplTest {
	@Test
	public void positiveRangeSucess() {
		Assert.assertEquals("2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14", new FizzBuzzImpl(new FizzbuzzFactory()).calculate(2, 14));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void rangeException() {
		new FizzBuzzImpl(new FizzbuzzFactory()).calculate(3, 1);
	}
}
